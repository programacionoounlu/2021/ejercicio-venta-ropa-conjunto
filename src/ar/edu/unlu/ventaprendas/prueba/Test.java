package ar.edu.unlu.ventaprendas.prueba;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unlu.ventaprendas.conjuntos.Conjunto;
import ar.edu.unlu.ventaprendas.conjuntos.ConjuntoConDescuento;
import ar.edu.unlu.ventaprendas.conjuntos.ConjuntoDosPorUno;
import ar.edu.unlu.ventaprendas.interfaces.Vendible;
import ar.edu.unlu.ventaprendas.prendas.Camisa;
import ar.edu.unlu.ventaprendas.prendas.Prenda;
import ar.edu.unlu.ventaprendas.prendas.Remera;
import ar.edu.unlu.ventaprendas.prendas.Sweater;
import ar.edu.unlu.ventaprendas.tarjetas.Tarjeta;
import ar.edu.unlu.ventaprendas.tarjetas.TarjetaDorada;

public class Test {
	public static void main(String args[]) {
		Tarjeta tarjeta = new Tarjeta();
		TarjetaDorada tarjetaDorada = new TarjetaDorada();
		
		Prenda prenda = new Prenda(100.00);
		System.out.println(prenda.getPrecioDeLista() + " debería ser $110.00");
		Camisa camisaMangaCorta = new Camisa(100.00, false);
		System.out.println("Camisa Manga Corta sin tarjeta " + camisaMangaCorta.calcularPrecio() + " debería ser $110.00");
		System.out.println("Camisa Manga Corta con tarjeta " + camisaMangaCorta.calcularPrecio(tarjeta) + " debería ser $108.90");
		System.out.println("Camisa Manga Corta con tarjeta dorada " + camisaMangaCorta.calcularPrecio(tarjetaDorada) + " debería ser $108.35");
		Camisa camisaMangaLarga = new Camisa(100.00, true);
		System.out.println(camisaMangaLarga.calcularPrecio() + " debería ser $115.00");
		Remera remera = new Remera(110.00);
		System.out.println(remera.calcularPrecio() + " debería ser $220.00");
		Sweater sweater = new Sweater(100.00);
		System.out.println(sweater.calcularPrecio() + " debería ser $108.00");
		
		Prenda a = new Remera(100.00);
		Prenda b = new Sweater(100.00);
		Prenda c = new Prenda(100.00);
		Prenda d = new Camisa(100.00, true);
		
		Conjunto conjuntoRemeraCamisa2x1 = new ConjuntoDosPorUno(remera, camisaMangaCorta);
		System.out.println("conjuntoRemeraCamisa2x1 " + conjuntoRemeraCamisa2x1.calcularPrecio() + " nos debería dar " + remera.calcularPrecio());
		System.out.println("conjuntoRemeraCamisa2x1 con tarjeta " + conjuntoRemeraCamisa2x1.calcularPrecio(tarjeta) + " nos debería dar " + (remera.calcularPrecio() * 0.99));
		Conjunto conjuntoRemeraRemera2x1 = new ConjuntoDosPorUno(remera, remera);
		System.out.println("conjuntoRemeraRemera2x1 " + conjuntoRemeraRemera2x1.calcularPrecio() + " nos debería dar " + remera.calcularPrecio());
		double descuento = 0.3;
		Conjunto conjuntoRemeraCamisaConDescuento = new ConjuntoConDescuento(remera, camisaMangaLarga, descuento);
		double precioTotal = (remera.calcularPrecio() + camisaMangaLarga.calcularPrecio()) * (1 - descuento);
		System.out.println("conjuntoRemeraCamisaConDescuento " + conjuntoRemeraCamisaConDescuento.calcularPrecio() + " nos debería dar " + precioTotal);
		
		//System.out.println(a.calcularPrecio() + " debería ser $220.00");
		//System.out.println(b.calcularPrecio() + " debería ser $108.00");
		//System.out.println(c.calcularPrecio() + " debería ser $110.0");
		if(d instanceof Camisa) {
			Camisa x = (Camisa) d;
			//System.out.println("d es una camisa");
		}
		List<Vendible> vendibles = new ArrayList<>();
		vendibles.add(camisaMangaCorta);
		vendibles.add(camisaMangaLarga);
		vendibles.add(remera);
		vendibles.add(sweater);
		vendibles.add(conjuntoRemeraRemera2x1);
		vendibles.add(conjuntoRemeraCamisa2x1);
		vendibles.add(conjuntoRemeraCamisaConDescuento);
		
		for (Vendible vendible : vendibles) {
			System.out.print("El precio de la prenda es de $ " + vendible.calcularPrecio());
			if(vendible instanceof Camisa) {
				Camisa k = (Camisa) vendible;
				if(k.isMangaLarga()) {
					System.out.println(" y es manga larga");
				}else {
					System.out.println(" y no es manga larga");
				}
			}
		}
	}
}
