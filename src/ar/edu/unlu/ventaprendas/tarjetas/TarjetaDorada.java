package ar.edu.unlu.ventaprendas.tarjetas;

public class TarjetaDorada extends Tarjeta {
	@Override
	public Double aplicarDescuento(Double monto) {
		return monto - (monto * 0.015);
	}
}
