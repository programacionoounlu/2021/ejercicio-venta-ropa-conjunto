package ar.edu.unlu.ventaprendas.prendas;


public class Camisa extends Prenda{
	
	private boolean mangaLarga;
	
	public Camisa(Double precioDeLista, boolean mangaLarga) {		
		super(precioDeLista);
		this.mangaLarga = mangaLarga;				
	}
	
	public boolean isMangaLarga() {
		return this.mangaLarga;
	}
	
	@Override
	public Double calcularPrecio() {
		Double precio = super.calcularPrecio();
		if(this.isMangaLarga()) {
			precio += precio * 0.05;
		}
		return precio;
	}
		
}
