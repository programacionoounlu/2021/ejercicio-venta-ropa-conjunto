package ar.edu.unlu.ventaprendas.conjuntos;

import ar.edu.unlu.ventaprendas.prendas.Prenda;
import ar.edu.unlu.ventaprendas.prendas.Remera;
import ar.edu.unlu.ventaprendas.tarjetas.Tarjeta;

public class ConjuntoConDescuento extends Conjunto {

	private Double descuento;

	public ConjuntoConDescuento(Remera remera, Prenda prenda, Double descuento) {
		super(remera, prenda);
		this.descuento = descuento;
	}

	@Override
	public Double calcularPrecio() {
		Double precioTotal = this.remera.calcularPrecio() + this.prenda.calcularPrecio();
		return precioTotal - (precioTotal * this.descuento);
	}

	@Override
	public Double calcularPrecio(Tarjeta tarjeta) {
		return tarjeta.aplicarDescuento(this.calcularPrecio());
	}

}
