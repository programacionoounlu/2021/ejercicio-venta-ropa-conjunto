package ar.edu.unlu.ventaprendas.conjuntos;

import ar.edu.unlu.ventaprendas.interfaces.Vendible;
import ar.edu.unlu.ventaprendas.prendas.Prenda;
import ar.edu.unlu.ventaprendas.prendas.Remera;

public abstract class Conjunto implements Vendible {
	protected Remera remera;
	protected Prenda prenda;
	
	public Conjunto(Remera remera, Prenda prenda) {
		this.remera = remera;
		this.prenda = prenda;
	}
}
