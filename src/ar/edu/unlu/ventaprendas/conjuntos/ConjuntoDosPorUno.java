package ar.edu.unlu.ventaprendas.conjuntos;

import ar.edu.unlu.ventaprendas.prendas.Prenda;
import ar.edu.unlu.ventaprendas.prendas.Remera;
import ar.edu.unlu.ventaprendas.tarjetas.Tarjeta;

public class ConjuntoDosPorUno extends Conjunto {
	public ConjuntoDosPorUno(Remera remera, Prenda prenda) {
		super(remera, prenda);
	}
	
	@Override
	public Double calcularPrecio() {
		Double precioRemera = remera.calcularPrecio();
		Double precioPrenda = prenda.calcularPrecio();
		if (precioRemera > precioPrenda) {
			return precioRemera;
		} else {
			return precioPrenda;
		}
	}

	@Override
	public Double calcularPrecio(Tarjeta tarjeta) {
		return tarjeta.aplicarDescuento(this.calcularPrecio());
	}

}
