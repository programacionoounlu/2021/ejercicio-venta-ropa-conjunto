package ar.edu.unlu.ventaprendas.interfaces;

import ar.edu.unlu.ventaprendas.tarjetas.Tarjeta;

public interface Vendible {
	Double calcularPrecio();
	Double calcularPrecio(Tarjeta tarjeta);
}
